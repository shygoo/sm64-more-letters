# SM64 More Letters Patch for armips

Imports the following colored characters from SM64 (J):
```
[J] [Q] [V] [X] [Z] [!] [!!]
[?] [&] [%] [Circle] [Key]
```

This patch is compatible with SM64 (U) ROMs extended with VL-Tone's extender or equivalent.

# Summary

* Appends image data from `more-letters.bin` to segment 02
* Updates segment-offset pointers in the character table
* Updates segment 02 loader addresses

# Requirements

* 0x1800 bytes of free space at 0x0081D364 (or `NUM_CHARS` * 0x200)
  
# Configuration

* Reduce `NUM_CHARS` in more-letters.asm if SM64 crashes